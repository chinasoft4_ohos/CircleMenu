package com.hitomi.cmlibrary;

/**
 * 菜单状态发生改变时的回调接口
 * <p>
 * Created by hitomi on 2016/10/9.
 */
public interface OnMenuStatusChangeListener {
    /**
     * onMenuOpened
     */
    void onMenuOpened();

    /**
     * onMenuClosed onMenuClosed
     */
    void onMenuClosed();
}
