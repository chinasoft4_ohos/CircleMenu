/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hitomi.circlemenu.slice;

import com.hitomi.circlemenu.ResourceTable;
import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;
import com.hitomi.cmlibrary.OnMenuStatusChangeListener;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Text;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;

public class MainAbilitySlice extends AbilitySlice {
    private CircleMenu circleMenu;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().setStatusBarColor(Color.getIntColor("#3b4dac"));
        circleMenu = (CircleMenu) findComponentById(ResourceTable.Id_circle_menu);
        Text title = (Text) findComponentById(ResourceTable.Id_title);
        title.setFont(Font.DEFAULT_BOLD);
        circleMenu.setMainMenu(Color.getIntColor("#CDCDCD"), ResourceTable.Graphic_icon_menu, ResourceTable.Graphic_icon_cancel)
                .addSubMenu(Color.getIntColor("#258CFF"), ResourceTable.Graphic_icon_home)
                .addSubMenu(Color.getIntColor("#30A400"), ResourceTable.Graphic_icon_search)
                .addSubMenu(Color.getIntColor("#FF4B32"), ResourceTable.Graphic_icon_notify)
                .addSubMenu(Color.getIntColor("#8A39FF"), ResourceTable.Graphic_icon_setting)
                .addSubMenu(Color.getIntColor("#FF6A00"),ResourceTable.Graphic_icon_gps)
                .setOnMenuSelectedListener(new OnMenuSelectedListener() {

                    @Override
                    public void onMenuSelected(int index) {}

                }).setOnMenuStatusChangeListener(new OnMenuStatusChangeListener() {

            @Override
            public void onMenuOpened() {}

            @Override
            public void onMenuClosed() {
            }

        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
