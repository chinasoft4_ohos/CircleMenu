# CircleMenu

#### 项目介绍
- 项目名称：CircleMenu
- 所属系列：openharmony的第三方组件适配移植
- 功能：CircleMenu 是一个精美别致支持定制的圆形菜单，可以有 0 到 8 个子菜单按钮，按钮背景色，图标都可以修改。CircleMenu 比较适合放在屏幕中间，以得到完好的展现
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v1.1.0

#### 效果演示
<img src="img/demo.gif"></img>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:CircleMenu:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

布局文件中：
```示例XML
  <com.hitomi.cmlibrary.CircleMenu
             ohos:id="$+id:circle_menu"
             ohos:top_margin="60vp"
             ohos:height="160vp"
             ohos:width="160vp"/>
```
Ability中：
```java
       circleMenu = (CircleMenu) findComponentById(ResourceTable.Id_circle_menu);
             circleMenu.setMainMenu(Color.getIntColor("#CDCDCD"), ResourceTable.Graphic_icon_menu, ResourceTable.Graphic_icon_cancel)
                     .addSubMenu(Color.getIntColor("#258CFF"), ResourceTable.Graphic_icon_home)
                     .addSubMenu(Color.getIntColor("#30A400"), ResourceTable.Graphic_icon_search)
                     .addSubMenu(Color.getIntColor("#FF4B32"), ResourceTable.Graphic_icon_notify)
                     .addSubMenu(Color.getIntColor("#8A39FF"), ResourceTable.Graphic_icon_setting)
                     .addSubMenu(Color.getIntColor("#FF6A00"),ResourceTable.Graphic_icon_gps)
                     .setOnMenuSelectedListener(new OnMenuSelectedListener() {
     
                         @Override
                         public void onMenuSelected(int index) {}
     
                     }).setOnMenuStatusChangeListener(new OnMenuStatusChangeListener() {
     
                 @Override
                 public void onMenuOpened() {}
     
                 @Override
                 public void onMenuClosed() {}
     
             });
```
Method

|           方法          |                                                             说明                                                             |
|:-----------------------:|:-----------------------------------------------------------------------------------------------------------------------------------:|
| setMainMenu                  | 设置主按钮(打开/关闭)的背景色，以及打开/关闭的图标。图标支持 Resource、Bitmap、Drawable 形式  | 
| addSubMenu                  | 添加一个子菜单项，包括子菜单的背景色以及图标 。图标支持 Resource、Bitmap、Drawable 形式        |
| openMenu            | 打开菜单 |
| closeMenu           | 关闭菜单  | 
| isOpened                 | 菜单是否打开，返回 boolean 值  | 
| setOnMenuSelectedListener                  | 设置选中子菜单项的监听器，回调方法会传递当前点击子菜单项的下标值，从 0 开始计算 | 
| setOnMenuStatusChangeListener            | 设置 CircleMenu 行为状态监听器，onMenuOpened 为菜单打开后的回调方法，onMenuClosed 为菜单关闭后的回调方法  | 

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

      Copyright 2016 Hitomis, Inc.

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
